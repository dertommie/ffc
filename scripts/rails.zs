recipes.addShaped(<Railcraft:part.tie>, [[null, null, null], [null, null, null ], [<terrafirmacraft:item.Log:*>,<terrafirmacraft:item.Log:*>, <terrafirmacraft:item.Log:*>]]);
recipes.addShapeless(<Railcraft:part.rail:2>, [<Railcraft:part.tie>]);
recipes.removeShaped(<Railcraft:tool.crowbar>);
recipes.addShaped(<Railcraft:tool.crowbar>, [[null, null, <terrafirmacraft:item.Copper Ingot>], [null, <terrafirmacraft:item.Copper Ingot>, null], [<terrafirmacraft:item.Copper Ingot>, null, null]]);
recipes.addShaped(<minecraft:furnace_minecart>, [[<terrafirmacraft:Torch>], [<minecraft:minecart>]]);
recipes.addShaped(<minecraft:minecart>, [[null, null, null], [<terrafirmacraft:item.Copper Ingot>,null,<terrafirmacraft:item.Copper Ingot>], [<terrafirmacraft:item.Copper Ingot>, <terrafirmacraft:item.Copper Ingot>, <terrafirmacraft:item.Copper Ingot>]]);
recipes.addShaped(<Railcraft:track:20176>.withTag({track: "railcraft:track.locking"}), [[<Railcraft:part.rail:2>, <minecraft:lever>, <Railcraft:part.rail:2>], [<Railcraft:part.rail:2>, null, <Railcraft:part.rail:2>], [<Railcraft:part.rail:2>, <Railcraft:part.railbed>, <Railcraft:part.rail:2>]]);
