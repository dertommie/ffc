recipes.removeShapeless(<antiqueatlas:emptyAntiqueAtlas>);
recipes.addShapeless(<antiqueatlas:emptyAntiqueAtlas> * 1, [<terrafirmacraft:item.Hide:*>, <minecraft:feather>]);
recipes.addShapeless(<minecraft:wool> * 1, [<terrafirmacraft:item.WoolCloth>]);
recipes.addShapeless(<terrafirmacraft:item.WoolCloth> * 1, [<minecraft:wool>]);
recipes.addShaped(<animalcrate:blockcrate>, [[<ore:plankWood>, <terrafirmacraft:item.Copper Ingot>, <ore:plankWood>], [<ore:plankWood>, <terrafirmacraft:item.Straw>, <ore:plankWood>], [<ore:plankWood>, <ore:plankWood>, <ore:plankWood>]]);
